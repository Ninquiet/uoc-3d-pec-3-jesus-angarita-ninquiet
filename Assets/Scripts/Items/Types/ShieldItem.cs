using UnityEngine;
using Utilities;

namespace Items.Types
{
    public class ShieldItem : ItemBase
    {
        [SerializeField] private int _shieldAmount;
        
        public int ShieldAmount => _shieldAmount;

        protected override void ItemPicked()
        {
            base.ItemPicked();
            NotificationController.ShowNotification("Incrementa Escudo en " + _shieldAmount, NotificationType.Shield);
        }
    }
}