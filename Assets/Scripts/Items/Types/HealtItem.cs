using UnityEngine;
using Utilities;

namespace Items.Types
{
    public class HealtItem : ItemBase
    {
        [SerializeField] private int _healtAmount;
        
        public int HealtAmount => _healtAmount;
        
        protected override void ItemPicked()
        {
            base.ItemPicked();
            NotificationController.ShowNotification("Curado " + _healtAmount, NotificationType.Health);
        }
    }
}