using DG.Tweening;
using Player;
using UnityEngine;

namespace Items
{
    public class ItemBase : MonoBehaviour
    {
        protected bool _itemPicked;
        private void OnTriggerEnter(Collider other)
        {
            if (_itemPicked)
                return;

            if (other.TryGetComponent<PlayerItemPicker>(out var playerItemPicker))
            {
                playerItemPicker.PickItem(this, ItemPicked);
            }
        }
        
        protected virtual void ItemPicked()
        {
            _itemPicked = true;
            Sequence scaleSequence = DOTween.Sequence()
                .Append(transform.DOScale(transform.localScale * 1.3f, 0.15f))
                .Append(transform.DOScale(Vector3.zero, 0.15f));
            
            Tweener rotationTweener = transform.DORotate(new Vector3(0, 360, 0), 0.3f, RotateMode.LocalAxisAdd)
                .SetEase(Ease.Linear)
                .SetLoops(-1, LoopType.Incremental);
            
            scaleSequence.OnComplete(() => {
                DOTween.Kill(transform); 
                Destroy(gameObject);
            });
        }
    }
}
