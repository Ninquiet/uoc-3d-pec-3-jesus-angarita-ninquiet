using System.Collections.Generic;
using Character.Life;
using Player;
using TMPro;
using UnityEngine;

namespace UI
{
    public class PlayerStatsUI : MonoBehaviour
    {
        [SerializeField] 
        private PlayerWeaponHandler _playerWeaponHandler;
        [SerializeField] 
        private LifeController _playerLife;
        [SerializeField]
        private PlayerKeysHandler _playerKeysHandler;
        [Header("TMP Texts")]
        [SerializeField]
        private TMP_Text _weaponName;
        [SerializeField]
        private TMP_Text _activeAmmo;
        [SerializeField]
        private TMP_Text _lifePoints;
        [SerializeField]
        private TMP_Text _shield;
        [SerializeField]
        private TMP_Text _keys;

        private void Awake()
        {
            _playerWeaponHandler.OnWeaponChanged += WeaponHasChanged;
            _playerWeaponHandler.OnActiveAmmoChanged += ActiveAmmoHasChanged;
            _playerLife.OnLifePointsChanged += LifePointsHasChanged;
            _playerLife.OnShieldChanged += ShieldHasChanged;
            _playerKeysHandler._onKeyAdded += KeysHasChanged;
            _playerKeysHandler._onKeyRemoved += KeysHasChanged;
        }

        private void ShieldHasChanged(int shieldAmmount)
        {
            _shield.text = shieldAmmount.ToString();
        }

        private void ActiveAmmoHasChanged(int newAmmo)
        {
            _activeAmmo.text = newAmmo.ToString();
        }

        private void WeaponHasChanged(string newWeaponName)
        {
            _weaponName.text = newWeaponName;
        }

        private void LifePointsHasChanged(int newValue)
        {
            _lifePoints.text = newValue.ToString();
        }
        
        private void KeysHasChanged(List<string> obj)
        {
            var newValue = "";
            foreach (var key in obj)
            {
                newValue += key + "\n";
            }
            _keys.text = newValue;
        }
    }
}