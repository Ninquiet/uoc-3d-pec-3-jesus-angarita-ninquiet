using Character.Life;
using DG.Tweening;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace UI
{
    public class GeneralUIController : MonoBehaviour
    {
        [SerializeField]
        private LifeController _playerLifeController;
        [SerializeField]
        private CanvasGroup _gameOverCanvasGroup;
        [SerializeField]
        private CanvasGroup _winCanvasGroup;
        [SerializeField]
        private InputActionReference _pauseButton;
        
        private bool _gameOverEnabled;
        private bool _playerWin;
        private bool _gamePaused;

        private void Awake()
        {
            _playerLifeController.OnDeath += OnPlayerDeath;
            _pauseButton.action.performed += _ => PauseGame();
            Cursor.lockState = CursorLockMode.Locked;
        }

        private void PauseGame()
        {
            if (_gameOverEnabled || _playerWin)
                return;
            
            _gamePaused = !_gamePaused;
            if (_gamePaused)
            {
                Time.timeScale = 0;
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
            else
            {
                Time.timeScale = 1;
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
        }

        private void OnPlayerDeath()
        {
            if (_gameOverEnabled || _playerWin)
                return;
            
            _gameOverEnabled = true;
            _gameOverCanvasGroup.DOFade(1, 1f).OnComplete(() =>
            {
                _gameOverCanvasGroup.interactable = true;
                _gameOverCanvasGroup.blocksRaycasts = true;
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            });
        }

        public void OnPlayerWin()
        {
            if (_gameOverEnabled || _playerWin)
                return;
            _playerWin = true;
            _winCanvasGroup.DOFade(1, 1f).OnComplete(() =>
            {
                _winCanvasGroup.interactable = true;
                _winCanvasGroup.blocksRaycasts = true;
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            });
        }
        
        public void RestartGame()
        {
            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.name);
        }

        public void ExitGame()
        {
            Application.Quit();
        }
    }
}