using Character.Life;
using UnityEngine;

namespace Weapons.Bullets
{
    public class BasicBullet : MonoBehaviour
    {
        [SerializeField] private float _speed;
        [SerializeField] private int _damage;
        
        private bool _isMoving;
        
        public void SetBulletDirection (Vector3 direction)
        {
            transform.forward = direction;
            _isMoving = true;
            Destroy(gameObject,4);
        }
        
        private void Update()
        {
            if (_isMoving)
            {
                transform.position += transform.forward * (_speed * Time.deltaTime);
            }
        }

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.TryGetComponent(out LifeController lifeController))
            {
                var collisionDirection = other.GetContact(0).point - transform.position;
                lifeController.TakeDamage(_damage, collisionDirection);
            }
        }
    }
}