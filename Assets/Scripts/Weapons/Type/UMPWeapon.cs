using Character.Life;
using UnityEngine;

namespace Weapons.Type
{
    public class UMPWeapon : WeaponBase
    {
        [SerializeField] 
        private Transform _aimTransform;
        [SerializeField] 
        private ParticleSystem _shootParticles;
        [SerializeField] 
        private ParticleSystem _bloodParticlesPrefab;
        [SerializeField] 
        private LayerMask characterLayer;
        [SerializeField]
        private float maxRayDistance = 100f;

        public override void EnableWeapon()
        {
            _aimTransform.gameObject.SetActive(true);
        }

        public override void DisableWeapon()
        {
            _aimTransform.gameObject.SetActive(false);
        }

        public override void Shoot()
        {
            _shootParticles.Stop();
            _shootParticles.Play();
            
            var hitInfo = CameraRaycaster.GetRaycastHitFromCameraCenter(maxRayDistance, characterLayer);
            if (hitInfo.HasValue)
            {
                RaycastHit hit = hitInfo.Value;
                if (hit.collider != null)
                {
                    LifeController lifeController = hit.collider.GetComponent<LifeController>();
                    if (lifeController != null)
                    {
                        lifeController.TakeDamage((int)_damage, hit.point - transform.position);
                        CreateBlood(hit);
                    }
                    else 
                        CreateDecal(hit);
                }
            }
            
            CameraEffects.ShakeCamera();
        }

        private void CreateBlood(RaycastHit hit)
        {
            var bloodObject = Instantiate(_bloodParticlesPrefab, hit.point, Quaternion.LookRotation(hit.normal));
            var bloodParticle = bloodObject.GetComponent<ParticleSystem>();
            bloodParticle.Play();
        }
    }
}