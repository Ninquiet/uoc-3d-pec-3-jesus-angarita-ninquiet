using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using Character.Life;
using System.Collections;

namespace NPC
{
    [RequireComponent(typeof(NavMeshAgent))]
    [RequireComponent(typeof(LifeController))]
    public class Pedestrian : MonoBehaviour
    {
        [SerializeField] 
        private float detectionRadius = 10f;
        [SerializeField] 
        private float fleeSpeed = 5f;
        [SerializeField] 
        private float walkSpeed = 2f;
        [SerializeField] 
        private float wanderRadius = 20f;
        [SerializeField] 
        private float wanderTimer = 5f;
        [SerializeField] 
        private LayerMask zombieLayer;
        [SerializeField] 
        private Animator animator;
        [SerializeField] 
        private GameObject SpawnOnDeath;

        private NavMeshAgent _agent;
        private LifeController _lifeController;
        private Collider _characterController;
        private float _timer;
        private bool _isDeath = false;
        private Vector3 _targetPosition;

        private void Awake()
        {
            _agent = GetComponent<NavMeshAgent>();
            _lifeController = GetComponent<LifeController>();
            _characterController = GetComponent<Collider>();

            _lifeController.OnDeath += HandleDeath;
        }

        private void Start()
        {
            animator.SetFloat("Forward", 0f);
            _timer = wanderTimer;
            SetNewRandomDestination();
        }

        private void Update()
        {
            if (_lifeController.LifePoints <= 0)
                return;

            _timer += Time.deltaTime;

            Collider[] zombies = Physics.OverlapSphere(transform.position, detectionRadius, zombieLayer)
                                       .Where(c => c.CompareTag("Zombie"))
                                       .ToArray();

            if (zombies.Length > 0)
            {
                Vector3 fleeDirection = GetFleeDirection(zombies);
                _agent.speed = fleeSpeed;
                _agent.SetDestination(transform.position + fleeDirection);
                animator.SetFloat("Forward", 1f);
            }
            else
            {
                if (_timer >= wanderTimer || Vector3.Distance(transform.position, _targetPosition) < 1f)
                {
                    SetNewRandomDestination();
                    _timer = 0;
                }

                _agent.speed = walkSpeed;
                if (_agent.velocity.sqrMagnitude > 0.1f)
                {
                    animator.SetFloat("Forward", 0.5f);
                }
                else
                {
                    animator.SetFloat("Forward", 0f);
                }
            }
        }

        private void SetNewRandomDestination()
        {
            _targetPosition = RandomNavSphere(transform.position, wanderRadius, -1);
            _agent.SetDestination(_targetPosition);
        }

        private Vector3 GetFleeDirection(Collider[] zombies)
        {
            Vector3 fleeDirection = Vector3.zero;

            foreach (Collider zombie in zombies)
            {
                fleeDirection += (transform.position - zombie.transform.position).normalized;
            }

            return fleeDirection.normalized;
        }

        private void HandleDeath()
        {
            if (_isDeath)
                return;
            _isDeath = true;
            animator.SetTrigger("Death");
            _characterController.enabled = false;
            _agent.isStopped = true;
            StartCoroutine(AfterDeathCoroutine());
        }

        private IEnumerator AfterDeathCoroutine()
        {
            yield return new WaitForSeconds(3f);
            Instantiate(SpawnOnDeath, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }

        public static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask)
        {
            Vector3 randomDirection = Random.insideUnitSphere * dist;
            randomDirection += origin;
            NavMeshHit navHit;
            NavMesh.SamplePosition(randomDirection, out navHit, dist, layermask);
            return navHit.position;
        }
    }
}
