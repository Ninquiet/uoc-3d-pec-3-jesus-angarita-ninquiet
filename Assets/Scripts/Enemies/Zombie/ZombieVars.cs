using System;
using NinquietGame.StateMachine;
using Sounds;
using UnityEngine;

namespace Enemies.Zombie
{
    [Serializable]
    public class ZombieVars : BaseVars
    {
        [SerializeField]
        private ZombieSounds _zombieSounds;
        public ZombieSounds ZombieSounds => _zombieSounds;
        [SerializeField] 
        private EnemyBaseAttack _baseAttack;
        public EnemyBaseAttack BaseAttack => _baseAttack;
        [SerializeField]
        public float _attackDamage = 1.0f;
        public float AttackDamage => _attackDamage;
        [SerializeField]
        private Animator _animator;
        public Animator Animator => _animator;
        [SerializeField]
        private float _moveSpeed = 1.0f;
        public float MoveSpeed => _moveSpeed;
        [SerializeField]
        private float _attackRange = 1.0f;
        public double AttackRange => _attackRange;

        [SerializeField] 
        private Transform[] _waypoints = new Transform[0];
        public Transform[] Waypoints => _waypoints;
            
        [NonSerialized] 
        public bool IsSeeingThePlayer;
        [NonSerialized] 
        public Vector3 TargetDirection = Vector3.zero;
        [NonSerialized]
        public Transform TargetTransform = null;
        [NonSerialized] 
        public Vector3 Velocity = Vector3.zero;
        
    }
}