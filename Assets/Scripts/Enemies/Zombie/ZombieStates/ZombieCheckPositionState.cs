namespace Enemies.Zombie.ZombieStates
{
    public class ZombieCheckPositionState : ZombieBaseState
    {
        public ZombieCheckPositionState(ZombieStateFactory zombieStateFactory, ZombieStateMachine ctx, bool isRootState) : base(zombieStateFactory, ctx, isRootState)
        {
        }

        public override void OnEnter()
        {
            _ctx.Vars().Animator.SetBool("Walking", true);
        }

        protected override void OnExit()
        {
            
        }

        protected override void OnUpdate()
        {
            _ctx.MoveToTarget();
        }

        protected override void CheckChangeStateConditions()
        {
            if (_ctx.GetDistanceToTarget() < 1.0f)
            {
                if (_ctx.Vars().Waypoints.Length > 0)
                {
                    ChangeState(_factory.WayPointState());
                }
                else if (_ctx.Vars().IsSeeingThePlayer)
                {
                    ChangeState(_factory.MoveState());
                }
                else
                {
                    ChangeState(_factory.IdleState());
                }
                ChangeState(_factory.IdleState());
            }
            
            if (_ctx.Vars().IsSeeingThePlayer)
            {
                ChangeState(_factory.MoveState());
            }
        }
    }
}