using UnityEngine;

namespace Enemies.Zombie.ZombieStates
{
    public class ZombieWayPointState : ZombieBaseState
    {
        int _currentWaypointIndex = 0;
        public ZombieWayPointState(ZombieStateFactory zombieStateFactory, ZombieStateMachine ctx, bool isRootState) : base(zombieStateFactory, ctx, isRootState)
        {
        }

        public override void OnEnter()
        {
            if (_ctx.Vars().Waypoints.Length == 0)
            {
                ChangeState(_factory.IdleState());
                return;
            }

            _currentWaypointIndex = GetCloserWaypoint();
            _ctx.Vars().TargetTransform = _ctx.Vars().Waypoints[_currentWaypointIndex];
            _ctx.Vars().Animator.SetBool("Walking", true);
        }

        protected override void OnExit()
        {
            
        }

        protected override void OnUpdate()
        {
            if (HasReachedWaypoint())
            {
                _currentWaypointIndex = (_currentWaypointIndex + 1) % _ctx.Vars().Waypoints.Length;
                _ctx.Vars().TargetTransform = _ctx.Vars().Waypoints[_currentWaypointIndex];
            }
    
            if (_ctx.Vars().TargetTransform == null)
                return;
            _ctx.Vars().TargetDirection = _ctx.Vars().TargetTransform.position;
            _ctx.MoveToTarget();
        }

        private bool HasReachedWaypoint()
        {
            float reachThreshold = 1.0f; // Threshold distance to consider the waypoint reached
            return Vector3.Distance(_ctx.transform.position, _ctx.Vars().Waypoints[_currentWaypointIndex].position) <= reachThreshold;
        }

        protected override void CheckChangeStateConditions()
        {
            if (_ctx.Vars().IsSeeingThePlayer)
            {
                ChangeState(_factory.MoveState());
            }
        }
        
        private int GetCloserWaypoint()
        {
            int closestIndex = 0;
            float closestDistance = float.MaxValue;
            for (int i = 0; i < _ctx.Vars().Waypoints.Length; i++)
            {
                float distance = Vector3.Distance(_ctx.transform.position, _ctx.Vars().Waypoints[i].position);
                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    closestIndex = i;
                }
            }
            return closestIndex;
        }
    }
}