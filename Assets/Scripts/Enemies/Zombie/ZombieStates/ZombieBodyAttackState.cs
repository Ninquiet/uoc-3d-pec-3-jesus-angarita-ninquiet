using UnityEngine;

namespace Enemies.Zombie.ZombieStates
{
    public class ZombieBodyAttackState : ZombieBaseState
    {
        private bool _attackPerformed;
        private float _timeSinceAttackAnimationStarted;
        private float TimeThatHasPast => Time.time - _timeSinceAttackAnimationStarted;
        public ZombieBodyAttackState(ZombieStateFactory zombieStateFactory, ZombieStateMachine ctx, bool isRootState) : base(zombieStateFactory, ctx, isRootState)
        {
        }

        public override void OnEnter()
        {
            _ctx.Vars().Animator.SetBool("Walking", false);
            _ctx.Vars().Animator.SetTrigger("Attack");
            _timeSinceAttackAnimationStarted = Time.time;
        }

        protected override void OnExit()
        {
            
        }

        protected override void OnUpdate()
        {
            _ctx.RotateTowardsTarget();
            if (!_attackPerformed  && TimeThatHasPast > 0.3f)
            {
                _ctx.Vars().BaseAttack.PerformAttack(_ctx.Vars()._attackDamage);
                _ctx.Vars().ZombieSounds.PlaySound( _ctx.Vars().ZombieSounds._AttackSound);
                _attackPerformed = true;
            }
        }

        protected override void CheckChangeStateConditions()
        {
            if (!_ctx.Vars().IsSeeingThePlayer)
            {
                ChangeState(_ctx.GetDefaultState());
            }
            
            if (TimeThatHasPast > 1.5f)
            {
                ChangeState(_factory.IdleState());
            }
        }
    }
}