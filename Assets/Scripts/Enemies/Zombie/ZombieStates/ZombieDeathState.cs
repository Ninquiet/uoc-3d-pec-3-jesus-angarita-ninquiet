using UnityEngine;

namespace Enemies.Zombie.ZombieStates
{
    public class ZombieDeathState : ZombieBaseState
    {
        private float _disappearTime = 5f;
        public ZombieDeathState(ZombieStateFactory zombieStateFactory, ZombieStateMachine ctx, bool isRootState) : base(zombieStateFactory, ctx, isRootState)
        {
        }

        public override void OnEnter()
        {
            _ctx.Destroy(_disappearTime);
            _ctx.Vars().Animator.SetTrigger("IsDead");
        }

        protected override void OnExit()
        {
            
        }

        protected override void OnUpdate()
        {
        }

        protected override void CheckChangeStateConditions()
        {
        }
    }
}