using System;
using UnityEngine;

namespace Enemies
{
    public abstract class EnemyBaseAttack : MonoBehaviour
    {
        public abstract void PerformAttack(float damange);
    }
}