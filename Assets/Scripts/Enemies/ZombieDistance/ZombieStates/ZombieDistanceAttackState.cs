using Enemies.Zombie;
using Enemies.Zombie.ZombieStates;
using UnityEngine;
using Weapons.Bullets;

namespace Enemies.ZombieDistance
{
    public class ZombieDistanceAttackState : ZombieBaseState
    {
        private float _timeSinceAttackAnimationStarted;
        private ZombieDistanceVars _distanceVars;
        private float _timeBetweenShoots = 0.7f;
        private float _timeSinceLastShoot;
        private float TimeThatHasPast => Time.time - _timeSinceAttackAnimationStarted;
        
        private bool _hasShot;
        public ZombieDistanceAttackState(ZombieStateFactory zombieStateFactory, ZombieStateMachine ctx, bool isRootState) : base(zombieStateFactory, ctx, isRootState)
        {
            _distanceVars = _ctx.Vars() as ZombieDistanceVars;
        }

        public override void OnEnter()
        {
            _ctx.Vars().Animator.SetBool("Walking", false);
            _ctx.Vars().Animator.SetBool("DistanceAttacking", true);
            _ctx.Vars().Animator.SetTrigger("Attack");
            _timeSinceAttackAnimationStarted = Time.time;
            _timeSinceLastShoot = Time.time;
        }

        protected override void OnExit()
        {
            _ctx.Vars().Animator.SetBool("DistanceAttacking", false);
        }

        protected override void OnUpdate()
        {
            _ctx.RotateTowardsTarget();
            var canShootAgain = Time.time - _timeSinceLastShoot > _timeBetweenShoots;
            if (canShootAgain)
            {
                ShootBullet();
                _timeSinceLastShoot = Time.time;
            }
        }

        protected override void CheckChangeStateConditions()
        {
            if (!_distanceVars.IsSeeingThePlayer)
            {
                ChangeState(_ctx.GetDefaultState());
            }

            if (_hasShot && _ctx.GetDistanceToTarget() > 4.5)
            {
                ChangeState( _factory.MoveState());
            }
        }

        private void ShootBullet()
        {
            var bulletPrefab = _distanceVars.BulletPrefab;
            var spawnPoint = _distanceVars.ShootOrigin.position;
            BasicBullet bullet = GameObject.Instantiate(bulletPrefab, spawnPoint, Quaternion.identity);
            bullet.SetBulletDirection(_distanceVars.ShootOrigin.forward);
            
            _hasShot = true;
        }
    }
}