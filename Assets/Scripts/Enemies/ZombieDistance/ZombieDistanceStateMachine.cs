using UnityEngine;

namespace Enemies.Zombie.ZombieStates
{
    public class ZombieDistanceStateMachine : ZombieStateMachine
    {
        [SerializeField] 
        private ZombieDistanceVars _distanceVars;
       
        private ZombieDistanceStateFactory _distanceStateFactory;
        public override ZombieVars Vars()
        {
            return _distanceVars;
        }
        
        public override ZombieStateFactory Factory()
        {
            return _distanceStateFactory ??= new ZombieDistanceStateFactory(this);
        }
    }
}