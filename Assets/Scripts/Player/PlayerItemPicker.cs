using System;
using Character.Life;
using Items;
using Items.Types;
using UnityEngine;

namespace Player
{
    public class PlayerItemPicker : MonoBehaviour
    {
        [SerializeField] 
        private LifeController _lifeController;
        [SerializeField] 
        private PlayerWeaponHandler _playerWeaponHandler;
        [SerializeField] 
        private PlayerKeysHandler _playerKeysHandler;

        public void PickItem(ItemBase item, Action ItemPicked = null)
        {
            if (item is KeyItem keyItem)
            {
                PickKey(keyItem, ItemPicked);
            }
            else if (item is AmmoItem ammoItem)
            {
                PickAmmo(ammoItem,ItemPicked);
            }
            else if (item is HealtItem healthItem)
            {
                PickHealt(healthItem,ItemPicked);
            }
            else if (item is ShieldItem shieldItem)
            {
                PickShield(shieldItem,ItemPicked);
            }
        }

        private void PickShield(ShieldItem shieldItem, Action itemPicked)
        {
            _lifeController.IncreaseShield(shieldItem.ShieldAmount);
            itemPicked?.Invoke();
        }

        private void PickKey(KeyItem keyItem, Action ItemPicked = null)
        {
            _playerKeysHandler.AddKey(keyItem.KeyId);
            ItemPicked?.Invoke();
        }

        private void PickAmmo(AmmoItem weaponItem, Action ItemPicked = null)
        {
            _playerWeaponHandler.AddMunition(weaponItem.WeaponType, weaponItem.AmmoAmount);
            ItemPicked?.Invoke();
        }

        private void PickHealt(HealtItem healthItem, Action ItemPicked = null)
        {
            _lifeController.Heal(healthItem.HealtAmount);
            ItemPicked?.Invoke();
        }
    }
}