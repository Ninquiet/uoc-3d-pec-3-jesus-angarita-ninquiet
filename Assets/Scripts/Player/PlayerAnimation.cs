using DG.Tweening;
using UnityEngine;
using UnityEngine.Animations.Rigging;

namespace Player
{
    [RequireComponent(typeof(Animator))]
    public class PlayerAnimation : MonoBehaviour
    {
        [SerializeField] private Rig _armRig;
        [SerializeField] private float _rigTransitionDuration = 0.5f; // Duration for the rig weight transition

        private Animator _animator;
        private Tween _forwardTween;
        private Tween _aimLayerTween;
        private Tween _rigTween;
        private float _forwardValue = 0f;
        private bool _isRunning = false;
        private bool _isAiming = false;

        private void Awake()
        {
            _animator = GetComponent<Animator>();
        }

        public void SetMoveDirection(Vector3 moveDirection)
        {
            if (moveDirection.sqrMagnitude >= 0.01f)
            {
                if (_forwardTween != null) 
                    _forwardTween.Kill();
                
                var forwardValue = _isRunning ? (_isAiming? 0.5f: 1f) : 0.5f;
                _forwardTween = DOTween.To(() => _forwardValue, x => _forwardValue = x, forwardValue, 0.1f);
            }
            else
            {
                if (_forwardTween != null) _forwardTween.Kill();
                _forwardTween = DOTween.To(() => _forwardValue, x => _forwardValue = x, 0f, 0.1f);
            }

            _animator.SetFloat("Forward", _forwardValue);
        }

        public void SetAimState(bool isAiming)
        {
            _isAiming = isAiming;
            float targetWeight = isAiming ? 0.75f : 0f;

            if (_aimLayerTween != null) _aimLayerTween.Kill();
            _aimLayerTween = DOTween.To(() => _animator.GetLayerWeight(_animator.GetLayerIndex("Aim")), 
                x => _animator.SetLayerWeight(_animator.GetLayerIndex("Aim"), x), 
                targetWeight, _rigTransitionDuration);

            if (_rigTween != null) _rigTween.Kill();
            _rigTween = DOTween.To(() => _armRig.weight, x => _armRig.weight = x, isAiming ? 1f : 0f, _rigTransitionDuration);
        }

        public void SetDeath()
        {
            _animator.SetTrigger("Death");
        }

        public void SetRunning(bool isRunning)
        {
            _isRunning = isRunning;
        }
    }
}