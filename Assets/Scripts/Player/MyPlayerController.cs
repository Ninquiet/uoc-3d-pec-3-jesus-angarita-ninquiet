using Character.Life;
using Sounds;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Player
{
    [RequireComponent(typeof(PlayerMovement))]
    [RequireComponent(typeof(PlayerAnimation))]
    public class MyPlayerController : MonoBehaviour
    {
        [SerializeField]
        private PlayerSounds _playerSounds;
        [SerializeField] 
        private LifeController _lifeController;
        [SerializeField] 
        private float _enemyPushForce = 3f;
        [Header("Weapons References")]
        [SerializeField]
        private PlayerWeaponHandler _playerWeaponHandler;
        [SerializeField]
        private InputActionReference _shootAction;
        [SerializeField]
        private InputActionReference _setFirstWeaponAction;
        [SerializeField]
        private InputActionReference _setSecondWeaponAction;

        private bool _beingPushed;
        private Vector3 _pushForce;
        private float _timeSincePushed;
        private bool _shooting;

        [SerializeField] private InputActionReference moveReference;
        [SerializeField] private InputActionReference lookReference;
        [SerializeField] private InputActionReference aimReference;

        private PlayerMovement _playerMovement;
        private CameraController _cameraController;
        private PlayerAnimation _playerAnimation;
        private Vector2 _moveInput;
        private Rigidbody _rigidbody;
        private bool _isAiming;

        
        public void GetIntoCar(Transform carTransform)
        {
            gameObject.SetActive(false);
            _cameraController.SetCarCameraAim(carTransform);
            _cameraController.ActivateCarCamera(true);
        }
        
        public void GetOutCar(Transform triggerTransform)
        {
            gameObject.SetActive(true);
            _cameraController.ActivateCarCamera(false);
            transform.position = triggerTransform.position;
            transform.rotation = triggerTransform.rotation;
        }
        
        private void Awake()
        {
            _playerMovement = GetComponent<PlayerMovement>();
            _cameraController = GetComponent<CameraController>();
            _playerAnimation = GetComponent<PlayerAnimation>();
            _rigidbody = GetComponent<Rigidbody>();

            _lifeController.OnDeath += HasDeath;
            _lifeController.OnBeingHit += HasBeenHit;

            _shootAction.action.performed += _ => _shooting = true;
            _shootAction.action.canceled += _ => _shooting = false;
            _setFirstWeaponAction.action.performed += _ => ChangeWeapon(0);
            _setSecondWeaponAction.action.performed += _ => ChangeWeapon(1);
        }

        private void OnEnable()
        {
            moveReference.action.performed += HandleMove;
            moveReference.action.canceled += HandleMove;
            aimReference.action.performed += HandleAim;
            aimReference.action.canceled += HandleAim;

            lookReference.action.Enable();
            moveReference.action.Enable();
            aimReference.action.Enable();
        }

        private void OnDisable()
        {
            moveReference.action.performed -= HandleMove;
            moveReference.action.canceled -= HandleMove;
            aimReference.action.performed -= HandleAim;
            aimReference.action.canceled -= HandleAim;

            lookReference.action.Disable();
            moveReference.action.Disable();
            aimReference.action.Disable();
        }

        private void HandleMove(InputAction.CallbackContext context)
        {
            _moveInput = context.ReadValue<Vector2>();
            _playerMovement.MoveInput = _moveInput;
        }

        private void HandleAim(InputAction.CallbackContext context)
        {
            _isAiming = context.ReadValueAsButton();
            _playerMovement.IsAiming = _isAiming;
        }

        private void FixedUpdate()
        {
            Vector3 moveDirection = new Vector3(_moveInput.x, 0, _moveInput.y);
            _playerAnimation.SetMoveDirection(moveDirection);

            CheckIfShoot();
            CheckIfPush();
        }

        private void HasBeenHit(Vector3 attackDirection)
        {
            _pushForce = attackDirection.normalized * _enemyPushForce;
            _timeSincePushed = Time.time;
            _beingPushed = true;
            _playerSounds.PlaySound(_playerSounds._hurtSound);
        }

        private void HasDeath()
        {
            _playerMovement.CanMove = false;
            _cameraController.CanRotate = false;
            _cameraController.SetDeathCamera();
            _playerAnimation.SetDeath();
        }

        private void CheckIfShoot()
        {
            if (!_shooting) 
                return;
            if (!_isAiming) 
                return;

            ShootCurrentWeapon();
        }

        private void ShootCurrentWeapon()
        {
            _playerWeaponHandler.TryShootCurrentWeapon();
        }

        private void ChangeWeapon(int index)
        {
            _playerWeaponHandler.ChangeWeapon(index);
        }

        private void CheckIfPush()
        {
            if (!_beingPushed) return;

            PushPlayer();

            if (Time.time - _timeSincePushed > 0.05f)
                _beingPushed = false;
        }

        private void PushPlayer()
        {
            _rigidbody.AddForce(_pushForce, ForceMode.Impulse);
        }
    }
}
