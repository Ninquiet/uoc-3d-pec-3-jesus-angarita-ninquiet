using UnityEngine;
using UnityEngine.InputSystem;

namespace Player
{
    [RequireComponent(typeof(Rigidbody))]
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private Transform followTarget;
        [SerializeField] private Transform player;
        [SerializeField] private float moveSpeed = 5.0f;
        [SerializeField] private float playerRotationSpeed = 720.0f;
        [SerializeField] private float damping = 0.1f; // Damping factor

        private Vector2 _moveInput;
        private Vector3 _moveDirection;
        private Rigidbody _rigidbody;
        private bool _isAiming;

        private Vector3 currentVelocity; // SmoothDamp current velocity
        private Vector3 _smoothMoveDirection;
        private bool _isRunning;
        
        public bool CanMove = true;

        public Vector2 MoveInput
        {
            set => _moveInput = value;
        }

        public bool IsAiming
        {
            set => _isAiming = value;
        }

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        private void FixedUpdate()
        {
            HandleMovement();
        }

        private void HandleMovement()
        {
            if (!CanMove) 
                return;
            
            Vector3 forward = followTarget.forward;
            forward.y = 0;
            forward.Normalize();

            Vector3 right = followTarget.right;
            right.y = 0;
            right.Normalize();

            if (_isAiming)
            {
                _moveDirection = right * _moveInput.x + forward * _moveInput.y;

                if (_moveInput.sqrMagnitude > 0.01f)
                {
                    Quaternion targetRotation = Quaternion.LookRotation(forward);
                }
            }
            else
            {
                _moveDirection = forward * _moveInput.y + right * _moveInput.x;

                if (_moveInput.sqrMagnitude > 0.01f)
                {
                    Quaternion targetRotation = Quaternion.LookRotation(_moveDirection);
                    player.rotation = Quaternion.Slerp(player.rotation, targetRotation, playerRotationSpeed * Time.fixedDeltaTime);
                }
            }

            var finalMoveSpeed = _isRunning ? (_isAiming? moveSpeed : moveSpeed * 1.5f) : moveSpeed;
            _smoothMoveDirection = Vector3.SmoothDamp(_smoothMoveDirection, _moveDirection * finalMoveSpeed , ref currentVelocity, damping);

            _rigidbody.velocity = new Vector3(_smoothMoveDirection.x, _rigidbody.velocity.y, _smoothMoveDirection.z);
        }

        public void RotatePlayer(Quaternion targetRotation)
        {
            Vector3 eulerRotation = targetRotation.eulerAngles;
            player.rotation = Quaternion.Slerp(player.rotation, Quaternion.Euler(0, eulerRotation.y, 0), playerRotationSpeed * Time.fixedDeltaTime);
        }

        public void SetRunning(bool readValueAsButton)
        {
            _isRunning = readValueAsButton;
        }
    }
}
