using System;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class PlayerKeysHandler : MonoBehaviour
    {
        private List<string> _keys = new List<string>();
        
        public Action<List<string>> _onKeyAdded;
        public Action<List<string>> _onKeyRemoved;

        private void Start()
        {
            _onKeyAdded?.Invoke(_keys);
        }

        public List<string> GetKeys()
        {
            return _keys;
        }
        
        public void AddKey(string keyId)
        {
            _keys.Add(keyId);
            _onKeyAdded?.Invoke(_keys);
        }
        
        public void RemoveKey(string keyId)
        {
            _keys.Remove(keyId);
            _onKeyRemoved?.Invoke(_keys);
        }
        
        public bool HasKey(string keyId)
        {
            return _keys.Contains(keyId);
        }
    }
}