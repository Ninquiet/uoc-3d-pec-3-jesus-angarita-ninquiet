using System;
using Cinemachine;
using UnityEngine;

namespace Utilities
{
    public class CameraChanger : MonoBehaviour
    {
        [SerializeField] 
        private CinemachineVirtualCamera _playerCamera;
        [SerializeField] 
        private CinemachineVirtualCamera _deathCamera;
        
        private static CinemachineVirtualCamera _playerCameraStatic;
        private static CinemachineVirtualCamera _deathCameraStatic;

        private void Awake()
        {
            _playerCameraStatic = _playerCamera;
            _deathCameraStatic = _deathCamera;
        }
        
        public static void SetPlayerCamera()
        {
            _playerCameraStatic.Priority = 1;
            _deathCameraStatic.Priority = 0;
        }

        public static void SetDeathCamera()
        {
            _playerCameraStatic.Priority = 0;
            _deathCameraStatic.Priority = 1;
        }
    }
}