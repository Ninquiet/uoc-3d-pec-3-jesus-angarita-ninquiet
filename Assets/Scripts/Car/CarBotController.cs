using UnityEngine;
using UnityEngine.AI;
using System.Collections.Generic;

namespace Car
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class CarBotController : MonoBehaviour
    {
        public Transform[] waypoints;
        private Queue<Transform> recentWaypoints = new Queue<Transform>(4);
        private NavMeshAgent navMeshAgent;
        private float avoidanceRadius = 5f; // Adjust as needed

        private void Awake()
        {
            navMeshAgent = GetComponent<NavMeshAgent>();
        }

        private void OnEnable()
        {
            MoveToNextWaypoint();
        }

        private void Update()
        {
            if (!navMeshAgent.enabled) return;

            if (!navMeshAgent.pathPending && navMeshAgent.remainingDistance < 0.5f)
            {
                MoveToNextWaypoint();
            }

            AvoidOtherCars();
        }

        private void MoveToNextWaypoint()
        {
            if (waypoints.Length == 0)
                return;

            Transform closestWaypoint = null;
            float closestDistance = Mathf.Infinity;

            foreach (Transform waypoint in waypoints)
            {
                if (recentWaypoints.Contains(waypoint))
                    continue;

                float distance = Vector3.Distance(transform.position, waypoint.position);
                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    closestWaypoint = waypoint;
                }
            }

            if (closestWaypoint != null)
            {
                if (recentWaypoints.Count == 4)
                {
                    recentWaypoints.Dequeue();
                }
                recentWaypoints.Enqueue(closestWaypoint);

                navMeshAgent.SetDestination(closestWaypoint.position);
            }
        }

        private void AvoidOtherCars()
        {
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, avoidanceRadius);
            foreach (Collider hitCollider in hitColliders)
            {
                if (hitCollider.gameObject != gameObject && hitCollider.CompareTag("Car"))
                {
                    Vector3 directionAwayFromCar = transform.position - hitCollider.transform.position;
                    navMeshAgent.Move(directionAwayFromCar.normalized * Time.deltaTime * navMeshAgent.speed);
                }
            }
        }
    }
}
