using UnityEngine;
using UnityEngine.InputSystem;
using Utilities;
using Player;

namespace Car
{
    public class CarTrigger : MonoBehaviour
    {
        [SerializeField]
        private CarController _carController;
        [SerializeField]
        private InputActionReference _enterCarAction;

        private MyPlayerController _playerController;
        private bool _isPlayerInRange = false;

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                _playerController = other.GetComponent<MyPlayerController>();
                if (_playerController != null)
                {
                    _isPlayerInRange = true;
                    NotificationController.ShowNotification("Presiona F para ingresar al auto.", NotificationType.Message);
                    _enterCarAction.action.Enable();
                    _enterCarAction.action.performed += OnEnterCarAction;
                    _carController.SetEntryTriggerTransform(transform); // Set entry trigger transform here
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                _isPlayerInRange = false;
                _enterCarAction.action.Disable();
                _enterCarAction.action.performed -= OnEnterCarAction;
            }
        }

        private void OnEnterCarAction(InputAction.CallbackContext context)
        {
            if (_isPlayerInRange && _playerController != null)
            {
                _carController.SetPlayerController(_playerController);
                _playerController.GetIntoCar(_carController.transform);
                _carController.UseByUser();
            }
        }

        private void OnDestroy()
        {
            _enterCarAction.action.performed -= OnEnterCarAction;
        }
    }
}