using Player;
using UnityEngine;
using UnityEngine.AI;

namespace Car
{
    public class CarController : MonoBehaviour
    {
        public CarBotController botController;
        public CarPlayerController playerController;
        private MyPlayerController _playerController;
        private Transform _entryTriggerTransform;
        private NavMeshAgent _navMeshAgent;

        private void Awake()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }

        public void SetPlayerController(MyPlayerController playerController)
        {
            _playerController = playerController;
        }

        public void SetEntryTriggerTransform(Transform entryTriggerTransform)
        {
            _entryTriggerTransform = entryTriggerTransform;
        }

        private void Start()
        {
            botController.enabled = false;
            playerController.enabled = false;
            UseByBot();
        }

        public void UseByUser()
        {
            botController.enabled = false;
            playerController.enabled = true;
            _navMeshAgent.enabled = false; // Disable NavMeshAgent

            playerController.SetExitAction(() => ExitCar());
        }

        public void UseByBot()
        {
            playerController.enabled = false;
            botController.enabled = true;
            _navMeshAgent.enabled = true; // Enable NavMeshAgent
        }

        private void ExitCar()
        {
            if (_playerController != null && _entryTriggerTransform != null)
            {
                _playerController.GetOutCar(_entryTriggerTransform);
                UseByBot();
            }
        }
    }
}