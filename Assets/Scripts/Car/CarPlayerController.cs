using UnityEngine;
using UnityEngine.InputSystem;

namespace Car
{
    public class CarPlayerController : MonoBehaviour
    {
        public float speed = 10f;
        public float rotationSpeed = 100f;
        private System.Action _exitAction;
        [SerializeField]
        private InputActionReference _exitCarAction;

        private bool _isPlayerInCar = false;

        private void OnEnable()
        {
            _exitCarAction.action.Enable();
            _exitCarAction.action.performed += OnExitCarAction;
        }

        private void OnDisable()
        {
            _exitCarAction.action.Disable();
            _exitCarAction.action.performed -= OnExitCarAction;
        }

        private void FixedUpdate()
        {
            if (_isPlayerInCar)
            {
                float move = Input.GetAxis("Vertical") * speed * Time.deltaTime;
                float rotation = Input.GetAxis("Horizontal") * rotationSpeed * Time.deltaTime;

                transform.Translate(0, 0, move);
                transform.Rotate(0, rotation, 0);
            }
        }

        private void OnExitCarAction(InputAction.CallbackContext context)
        {
            _exitAction?.Invoke();
            _isPlayerInCar = false;
        }

        public void SetExitAction(System.Action exitAction)
        {
            _exitAction = exitAction;
            _isPlayerInCar = true;
        }
    }
}