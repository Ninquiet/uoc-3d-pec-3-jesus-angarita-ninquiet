using System;
using Cinemachine;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Animations.Rigging;
using UnityEngine.InputSystem;

namespace Player
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField]
        private CinemachineVirtualCamera _mainCamera;
        [SerializeField]
        private CinemachineVirtualCamera _aimCamera;
        [SerializeField]
        private CinemachineVirtualCamera _deathCamera;
        [SerializeField]
        private CinemachineVirtualCamera _carCamera;
        [SerializeField]
        private InputActionReference _aimAction;
        [SerializeField]
        private InputActionReference _lookAction;
        [SerializeField]
        private InputActionReference _runAction;
        [SerializeField]
        private float _lookRotationSpeed = 5.0f;
        [SerializeField]
        private PlayerMovement _playerMovement;
        [SerializeField]
        private Transform _gunTargetAim;
        [SerializeField]
        private float _gunTargetDistance = 10.0f;
        [SerializeField]
        private Rig _armRig;
        [SerializeField]
        private PlayerAnimation _playerAnimation;

        private bool _isAiming = false;
        private Transform _playerTransform;
        private Vector2 _lookInput;
        private Quaternion _currentRotation;

        private Vector2 _smoothedLookInput;
        private Vector2 _lookInputVelocity;
        private bool _isDeath;
        
        public bool CanRotate = true;
        private Transform _carCameraAim;
        
        public Action<bool> OnAiming; // impementa en la UI para que aparezca yt desaparezca el circulito de apuntado

        private void OnEnable()
        {
            _aimAction.action.performed += HandleAim;
            _aimAction.action.canceled += HandleAim;
            _lookAction.action.performed += HandleLook;
            _lookAction.action.canceled += HandleLook;
            _runAction.action.performed += Running;
            _runAction.action.canceled += Running;
            _aimAction.action.Enable();
            _lookAction.action.Enable();
        }

        private void Running(InputAction.CallbackContext context)
        {
            _playerAnimation.SetRunning(context.ReadValueAsButton());
            _playerMovement.SetRunning(context.ReadValueAsButton());
        }

        private void OnDisable()
        {
            _aimAction.action.performed -= HandleAim;
            _aimAction.action.canceled -= HandleAim;
            _lookAction.action.performed -= HandleLook;
            _lookAction.action.canceled -= HandleLook;
            _aimAction.action.Disable();
            _lookAction.action.Disable();
        }

        private void HandleAim(InputAction.CallbackContext context)
        {
            if (_isDeath)
                return;
            
            _isAiming = context.ReadValueAsButton();
            _mainCamera.Priority = _isAiming ? 9 : 11;
            _aimCamera.Priority = _isAiming ? 11 : 9;

            _playerAnimation.SetAimState(_isAiming);
        }

        private void HandleLook(InputAction.CallbackContext context)
        {
            _lookInput = context.ReadValue<Vector2>();
        }

        private void Update()
        {
            if (_lookAction != null)
            {
                _lookInput = _lookAction.action.ReadValue<Vector2>();
                _smoothedLookInput = Vector2.SmoothDamp(_smoothedLookInput, _lookInput, ref _lookInputVelocity, 0.1f);
            }
        }

        private void FixedUpdate()
        {
            if (!CanRotate)
                return;
            
            if (_isAiming)
            {
                if (_playerTransform == null)
                {
                    _playerTransform = _aimCamera.Follow;
                }

                float deltaTime = Time.fixedDeltaTime;

                Quaternion targetRotationY = Quaternion.Euler(0, _smoothedLookInput.x * _lookRotationSpeed, 0);
                Quaternion targetRotationX = Quaternion.Euler(_smoothedLookInput.y * _lookRotationSpeed, 0, 0);

                _currentRotation = _playerTransform.rotation;
                _currentRotation = Quaternion.Lerp(_currentRotation, _currentRotation * targetRotationY, deltaTime * _lookRotationSpeed);
                _currentRotation = Quaternion.Lerp(_currentRotation, _currentRotation * targetRotationX, deltaTime * _lookRotationSpeed);

                var angles = _currentRotation.eulerAngles;
                angles.z = 0;
                angles.x = Mathf.Clamp(angles.x > 180 ? angles.x - 360 : angles.x, -40f, 40f);
                _currentRotation = Quaternion.Euler(angles);
                _playerTransform.rotation = _currentRotation;
                _playerMovement.RotatePlayer(_currentRotation);
                UpdateGunTargetAimPosition();
            }
            else
            {
                Transform followTarget = _mainCamera.Follow;
                float deltaTime = Time.fixedDeltaTime;

                Quaternion targetRotationY = Quaternion.Euler(0, _smoothedLookInput.x * _lookRotationSpeed, 0);
                Quaternion targetRotationX = Quaternion.Euler(_smoothedLookInput.y * _lookRotationSpeed, 0, 0);

                _currentRotation = followTarget.rotation;
                _currentRotation = Quaternion.Lerp(_currentRotation, _currentRotation * targetRotationY, deltaTime * _lookRotationSpeed);
                _currentRotation = Quaternion.Lerp(_currentRotation, _currentRotation * targetRotationX, deltaTime * _lookRotationSpeed);

                var angles = _currentRotation.eulerAngles;
                angles.z = 0;
                angles.x = Mathf.Clamp(angles.x > 180 ? angles.x - 360 : angles.x, -40f, 40f);
                _currentRotation = Quaternion.Euler(angles);

                followTarget.rotation = _currentRotation;
            }
        }

        private void UpdateGunTargetAimPosition()
        {
            CinemachineComponentBase aimComponent = _aimCamera.GetCinemachineComponent<CinemachineComponentBase>();
            Vector3 targetPosition = aimComponent.VcamState.FinalPosition + aimComponent.VcamState.FinalOrientation * Vector3.forward * _gunTargetDistance;
            _gunTargetAim.position = targetPosition;
        }

        public void SetDeathCamera()
        {
            _isDeath = true;
            _mainCamera.Priority = 9;
            _deathCamera.Priority = 15;
        }

        public void SetCarCameraAim(Transform carCameraAim)
        {
            _carCameraAim = carCameraAim;
            _carCamera.Follow = carCameraAim;
        }
        
        public void ActivateCarCamera(bool active)
        {
            _carCamera.Priority = active ? 12 : 2;
        }
    }
}
