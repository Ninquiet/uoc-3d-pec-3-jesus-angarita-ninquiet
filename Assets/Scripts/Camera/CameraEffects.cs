using Cinemachine;
using DG.Tweening;
using UnityEngine;

public class CameraEffects : MonoBehaviour
{
    [SerializeField]
    private CinemachineImpulseSource _impulseSource;
    
    private static CinemachineImpulseSource _staticImpulseSource;
    private void Awake()
    {
        _staticImpulseSource = _impulseSource;
    }
    
    public static void ShakeCamera()
    {
        _staticImpulseSource.GenerateImpulse();
    }
}