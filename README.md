# Programación 3D  Entrega de la actividad R3

# INQUILINO MALDITO V2
![Inquilino Maldito](https://i.ibb.co/TkkHXDp/image-2024-06-19-003654315.png)

# Video:
https://youtu.be/tZc6Uh_3KHw

# Build:
Puedes jugar el juego acá:
https://simmer.io/@Ninquiet/inquilinomaldito-beta-2

# Resumen:
Es un juego en el que debemos acabar con los zombies y escapar, estos zombies no son solo estaticos sino que tienen un sistema de movimiento bastante logrado

# Funcionamiento:

### Vista en Tercera Persona:
Implementamos una vista en tercera persona que permite al jugador tener una perspectiva más amplia del entorno, ahora en tercera persona, el jugador puede apuntar, y este apuntado es asistido (en la animación) por el sistema de Rigging de Unity y algunas animaciones custom, gran parte del reto fue adaptar el sistema que habia ya creado a un sistema en tercera persona, el cual es totalmente diferente y tiene sus diferentes limitaciones, el personaje puede ahora correr tambien y tiene diferentes tipos de capacidades, cómo tambien puede interactuar ahora con cosas, así que por ejemplo puede subirse a un coche y manejarlo. cómo tambien puede recoger todos los objetos.
![Nuevo punto de vista](https://i.ibb.co/CQ5CQbz/image-2024-06-18-232625793.png)

### Peatones:
Se añadieron peatones al juego que reaccionan al entorno. Estos peatones huyen de los zombies cercanos y tienen animaciones para caminar, correr y morir. Además, cuando un peatón muere, genera un zombie nuevo, estos peatones son autonomos, así que si no estan siendo perseguidos por zombies, caminan de forma aleatoria por los alrededores

![Peaton](https://i.ibb.co/SfbX8NQ/image-2024-06-18-232343034.png)

### Vehículos:
Se introdujeron vehículos manejables por el jugador. Estos vehículos están configurados para no chocar entre ellos, proporcionando una experiencia de conducción suave y más realista, estos coches fueron creados usando Probuilder, el jugador puede interactuar con estos y al interactuar el jugador es desactivado y el sistema de camaras es dirigido a seguir ahora a este vehiculo, que puede ser controlado entonces, y al oprimir nuevamente F el jugador este saldrá por la misma puerta por la que entró, el jugador tomará control nuevamente del personaje, y el automovil continuará con su trayecto auto dirigido.

![Vehicles](https://i.ibb.co/1zhdb1P/image-2024-06-18-232439222.png)

### Mejoras en el Sistema de Animaciones:
Se realizaron ajustes en el sistema de animaciones para asegurar transiciones más suaves entre estados, como caminar, correr, y estar parado. Esto se logró ajustando los parámetros en el Animator y mejorando las condiciones de transición.

### Una mini ciudad:
Cree una pequeña ciudad ayudado de Pro builder y algunos asets super simples, la idea era poder crear un espacio donde explorar cada una de las mecanicas del juego
![City](https://i.ibb.co/RjRFZB4/image-2024-06-18-233200295.png)

### Post Processado:

Implementé un poco de bloom y configuré un poco los colores en el post Procesado, todo esto para lograr un efecto un poco más siniestro.


### State Machine:

![Diagrama](https://i.ibb.co/T2JNs4j/image-2024-05-13-061540717.png)

Diseñe un sistema de State machine usando generics que me permite re utilizarlo en cualquier parte, incluso en otros proyectos luego de este, este sistema funciona con un StateMachine base que es el nucleo principal de control y la ultima desición, luego tenemos el State Factory que se encarga de organinzar y retornar los diferentes estados existentes, y por ultimo los BaseState (y sus variantes), que se encargan de gestionar los diferentes estados, cómo estar en IDLE luego caminar, luego moverse, y así sucesivamente.

Para hacer este sistema de una forma modular, cree un cuarto integrante que se encarga de gestionar las variables que son de vital importancia para los estados y sub estados, cómo para el state machine tambien, así nace Base Vars.

Teniendo esta estructura, la transicioné para que funcione con estructuras genericas, y de esta forma podria modificarla a mi gusto, con esto pude crear variantes cómo ZombieStateMachine, o más adelante ZombieDistanceStateMachine que hereda de StateMachine y solo modifica algunos estados y algunas variables (y así ese zombie no tuvo que ser escrito desde cero).

![Diagrama](https://i.ibb.co/cQwBh8V/image-2024-05-13-062838255.png)

(ejemplo de uso de estructura generica)
![Diagrama](https://i.ibb.co/FHfN77y/image-2024-05-13-062354817.png)


### Player:

El player no usa state machine, usa PlayerController que hace de base de control de una forma general, las partes importantes del personaje sonn el poder contnrolar su vida (cómo tambien la de los zombies usando LifeController), poder controlar cuantas llaves lleva puestas con KeysHandler, las armas que puede usar con PlayerWeaponHandler, o el poder recoger items con PlayerItemPicker.cs, entre muchos otros sub sistemas que sostienen el jugador.

### Armas y Ataque: 

La forma en que funciona el sistema de daño y ataque es que el personaje que ataca revisa si e objeto atacado posee un LifeController, si lo tiene entonces se notifica del daño, el lifeController a su vez usando un sistema de eventos, notifica a todas las clases interesadas de los cambios, e incluso de la posible muerte del jugador.

El Zombie a distancia realiza el ataque desde su sistema de estados, sin embargo el zombie cuerpo a cuerpo hace uso de una tercera clase llamada EnemyBaseAttack, esto para poder tercerizar el proceso de ataque fisico que es un poco más complejo que el realizado por los zombies a distancia.

### La muerte: 
Para este efecto y muchos otros que se encuentran en el prototipo hice uso de Cinemachine, al realizar el cambio de camara y jugar con los valores en el UI se logra el efecto de que el jugador cae rendido al morir.

Por su lado, los Zombies manejan la muerte desde uno de sus estados tambien, 

 Inteligencia del enemigo: 
Traté de realizar el state machine de la forma más pulida que pude, así que hay cosas cómo que si el zombie tiene asignados waypoints, zombie seguirá estos waypoints llegado el caso de que pierda de vista al usuario, al momento de regresar al waypoint siempre calcula cual es el más cercano.

![Free Trees](https://i.ibb.co/McrJPcZ/image-2024-05-13-063645400.png) 

Para lograr el movimiento del enemigo usé un sistema de navmesh, interconectado con el StateMachine del zombie y controlado por los estados

Para la animación del zombie disparando usé un poco de blender.

# Effectos
Para una trasnción más limpia y pulida, hice uso de Cinemachine, y no solo esto sino tambien para efectos de camará cómo el shake, para el cual usé el sistema de impulsos.

Para el efecto de disparo usé el sistema de particulas, y algunas texturas basicas creadas en photoshop

# Objeetos

El sistema de objetos permite al jugador interactuar cón objetos de munición, objetos de vida, objetos de escudo, y objetos de llaves, estos ultimos són usados para abrir puertas.


![Objetcs](https://i.ibb.co/Zh6TK0W/image-2024-05-13-064412852.png)

# Notificaciones
Implementé un sistema de Notificaciones a modo de feedback, así el usuario tiene claridad en cuanto a si recogio o no la munición  cuanto recibió, si el objeto lo curó o le incrementó el escudo, e incluso para saber la razón por la cual una puerta no se abre, es la forma de interactuar con el usuario.

![Notificacion](https://i.ibb.co/KsKnt39/image-2024-05-13-064738708.png)

# UI
Para la interfaz de usuario usé el sistema de acciones ya creado, al subscribir a cada una de las clases (por ejemplo lifeController del player para recibir información de la vida, o PlayerWeaponHandler para recibir información de las armas), se pudo crear un sistema preciso de cada uno de los datos imporntantes a comunicar.

![UI](https://i.ibb.co/VDvBT29/image-2024-05-13-065250677.png)

# Sonidos
Para el sistema de sonido cree una base llamada BaseSoundPlayer que se encarga de reproducir en un AudioSource asignado un clip especifico, luego, de esta clase derivé PlayerSounds y ZombieSounds, estas clases guardan en si referencias de sonidos especificas que usaránm de esta forma no repetimos codigo y es bastante util.

# Assets usados:

![DoTween](https://assetstore.unity.com/packages/tools/animation/dotween-hotween-v2-27676)
Para hacer transiciones rapidamente.